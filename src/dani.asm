;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.0 #11195 (Linux)
;--------------------------------------------------------
	.module dani
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _printf
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;dani.c:12: int main(char **argv, int argc) {
;	---------------------------------
; Function main
; ---------------------------------
_main::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	hl, #-21
	add	hl, sp
	ld	sp, hl
;dani.c:16: *anyString = '\0';
	ld	hl, #0
	add	hl, sp
	ld	(hl), #0x00
;dani.c:18: for (i = 0; i < argc; i++) {
	ld	-6 (ix), l
	ld	-5 (ix), h
	ld	-4 (ix), l
	ld	-3 (ix), h
	ld	bc, #0x0000
00103$:
	ld	a, c
	sub	a, 6 (ix)
	ld	a, b
	sbc	a, 7 (ix)
	jp	PO, 00118$
	xor	a, #0x80
00118$:
	jp	P, 00101$
;dani.c:19: strcpy(anyString, argv[i]);
	ld	a, -6 (ix)
	ld	-2 (ix), a
	ld	a, -5 (ix)
	ld	-1 (ix), a
	ld	e, c
	ld	d, b
	sla	e
	rl	d
	ld	l, 4 (ix)
	ld	h, 5 (ix)
	add	hl, de
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
	ld	l, a
	push	bc
	ld	e, -2 (ix)
	ld	d, -1 (ix)
	xor	a, a
00121$:
	cp	a, (hl)
	ldi
	jr	NZ, 00121$
	pop	bc
;dani.c:20: printf("Argumento %i:%s\n\r", i, anyString);
	ld	e, -4 (ix)
	ld	d, -3 (ix)
	push	bc
	push	de
	push	bc
	ld	hl, #___str_0
	push	hl
	call	_printf
	ld	hl, #6
	add	hl, sp
	ld	sp, hl
	pop	bc
;dani.c:18: for (i = 0; i < argc; i++) {
	inc	bc
	jr	00103$
00101$:
;dani.c:23: return 0;
	ld	hl, #0x0000
;dani.c:24: }
	ld	sp, ix
	pop	ix
	ret
___str_0:
	.ascii "Argumento %i:%s"
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
